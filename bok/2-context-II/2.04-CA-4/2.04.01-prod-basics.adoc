
[[KLP-prod-mgmt-basics]]
==== Product Management Basics

*Description*

(((product management)))Before work, before operations, there must be a vision of the product. A preliminary vision may exist, but now as the organization grows, the Digital Practitioner needs to consider further how they will sustain that vision and establish an ongoing capability for realizing it. Like many other topics in this document, product management is a significant field in and of itself.

Historically, product management has _not_ been a major theme in enterprise IT management. IT systems started by serving narrow purposes, often “back-office” functions such as accounting or https://en.wikipedia.org/wiki/Material_requirements_planning[materials planning]. Mostly, such systems were managed as projects assembled on a temporary basis, resulting in the creation of a system to be “thrown over the wall” to operations. Product management, on the other hand, is concerned with the entire lifecycle. The product manager (or owner, in Scrum terms) cares about the vision, its execution, the market reaction to the vision (even if an internal market), the health, care, and feeding of the product, and the product’s eventual sunset or replacement.

(((Digital Transformation)))In the enterprise IT world, “third-party” vendors (e.g., IBM(R)) providing the back-office systems had product management approaches, but these were *external* to the IT operations. Nor were IT-based product companies as numerous 40 years ago as they are today; as noted in the section on xref:KLP-digital-xform[Digital Transformation], the digital component of modern products continues to increase to the point where it is often not clear whether a product is “IT” or not.

(((open-loop)))Reacting to market feedback and adapting product direction is an essential role of the product owner. In the older model, feedback was often unwelcome, as the project manager typically was committed to the xref:open-loop[open-loop dead reckoning] of the project plan and changing scope or direction was seen as a failure, more often than not.

Now, it is accepted that systems evolve, perhaps in unexpected directions. Rapidly testing, failing fast, learning, and pivoting direction are all part of the lexicon, at least for market-facing IT-based products. And even back-office IT systems with better understood scope are being managed more as systems (or products) with lifecycles, as opposed to transient projects. (See the xref:KLP-amazon-productization[Amazon discussion], below.)

[[KLP-prod-mgmt-definition]]
===== Defining Product Management

(((product, defined)))(((product management, defined)))In order to define product management, the product first needs to be defined. Previously, it was established that products are goods, services, or some combination, with some feature that provides value to some consumer. BusinessDictionary.com http://www.businessdictionary.com/definition/product.html[defines it as follows]:

[quote, BusinessDictionary.com]
[A Product is] A good, idea, method, information, object, or service created as a result of a process and serves a need or satisfies a want. It has a combination of tangible and intangible attributes (benefits, features, functions, uses) that a seller offers a buyer for purchase. For example, a seller of a toothbrush offers the physical product and also the idea that the consumer will be improving the health of their teeth. A good or service [must] closely meet the requirements of a particular market and yield enough profit to justify its continued existence.

Product _management_, according to the http://www.businessdictionary.com/definition/product-management.html#ixzz3bHCVkoWj[same source], is:

[quote, BusinessDictionary.com]
The organizational structure within a business that manages the development, marketing, and sale of a product or set of products throughout the product lifecycle. It encompasses the broad set of activities required to get the product to market and to support it thereafter.

Product management in the general sense often reports to the Chief Marketing Officer (CMO). It represents the fundamental strategy of the firm, in terms of its value proposition and viability. The product needs to reflect the enterprise’s strategy for creating and maintaining customers.

Product strategy for *internally-facing* products is usually _not_ defined by the enterprise CMO. If it is a back-office product, then “business within a business” thinking may be appropriate. (Even the payroll system run by IT for human resources is a “product”, in this view.) In such cases, there still is a need for someone to function as an “internal CMO” to the external “customers".

With xref:KLP-digital-xform[Digital Transformation](((Digital Transformation))), all kinds of products have increasing amounts of “IT” in them. This means that an understanding of IT, and ready access to any needed IT specialty skills, is increasingly important to the general field of product management. Product management includes R&D, which means that there is considerable uncertainty. This is of course also true of IT systems development.

Perhaps the most important aspect of product design is focusing on the user, and what they need. The concept of *outcome* is key. This is easier said than done. The general problem area is considered marketing, a core business school topic. Entire books have been written about the various tools and techniques for doing this, from focus groups to ethnographic analysis.

[[KLP-prod-mgmt-v-marketing]]
(((product management, _versus_ product marketing)))((("Cagan, Marty")))However, Marty Cagan recommends distinguishing product management from product marketing. He defines the two as follows:

_The product manager is responsible for defining — in detail — the product to be built and validating that product with real customers and users. The product marketing person is responsible for telling the world about that product, including positioning, messaging and pricing, managing the product launch, providing tools for the sales channel to market and sell the product, and for leading key programs such as online marketing and influencer marketing programs_ cite:[Cagan2008(10-11)].

Criticisms of overly marketing-driven approaches are discussed xref:jobs-to-be-done[below].


[[KLP-process-project-product]]
===== Process, Project, and Product Management

In the remainder of this document, we will continually encounter three major topics:

* Product Management (this Competency Area)
* Process Management (covered in xref:KLP-chap-coordination[])
* Project Management (covered in xref:KLP-chap-coordination[] and xref:chap-invest-mgmt[])

They have an important commonality: *all of them are concepts for driving results across organizations*. Here are some of the key differences between process, project, and product management in the context of digital services and systems:
(((product management _versus_ project/process)))(((project management _versus_ product/process)))(((process management _versus_ product/project)))

// break page for pdf
ifdef::backend-pdf[<<<]
.Process, Project, and Product Management
[cols="3*", options="header"]
|====
|Process|Project|Product
|Task-oriented|Deliverable-oriented|Outcome-oriented
|Repeatable with a high degree of certainty |Executable with a medium degree of certainty |Significant component of R&D, less certain of outcome — empirical approaches required
|Fixed time duration, relatively brief (weeks/months)|Limited time duration, often scoped to a year or less
|No specific time duration; lasts as long as there is a need
|Fixed in form, no changes usually tolerated|Difficult to change scope or direction, unless specifically set up to accommodate
|Must accommodate market feedback and directional change
|Used to deliver service value and operate system (the “Ops” in DevOps) |Often concerned with system design and construction, but typically not with operation (the “Dev” in DevOps)
|Includes service concept and system design, construction, operations, and retirement (both “Dev” and “Ops”)
|Process owners are concerned with adherence and continuous improvement of the process; otherwise can be narrow in perspective|Project managers are trained in resource and timeline management, dependencies, and scheduling; they are not typically incented to adopt a long-term perspective
|Product managers need to have project management skills as well as understanding market dynamics, feedback, building long-term organizational capability
|Resource availability and fungibility is assumed
|Resources are specifically planned for, but their commitment is temporary (team is “brought to the work”)
|Resources are assigned long-term to the product (work is “brought to the team”)
|====

The above distinctions are deliberately exaggerated, and there are of course exceptions (short projects, processes that take years). However, it is in the friction between these perspectives we see some of the major problems in modern IT management. For example, an activity which may be a one-time task or a repeatable process results in some work product - perhaps an artifact (see <<fig-process-400-c>>).

[[fig-process-400-c]]
.Activities Create Work Products
image::images/2_04-process.png[activities-work products, 400,,]

The consumer or stakeholder of that work product might be a project manager.

Project management includes concern for both the activities and the resources (people, assets, software) required to produce some deliverable (see <<fig-project-500-c>>).

[[fig-project-500-c]]
.Projects Create Deliverables with Resources and Activities
image::images/2_04-project.png[projects-deliverables, 500,,]

The consumer of that deliverable might be a product manager. Product management includes concern for projects and their deliverables, and their ultimate *outcomes*, either in the external market or internally (see <<fig-product-500-c>>).

[[fig-product-500-c]]
.Product Management may Use Projects
image::images/2_04-product.png[projects-deliverables, 500,,]

Notice that product management may directly access activities and resources. In fact, earlier-stage companies often do not formalize project management (see <<fig-productNoProject-600-c>>).

[[fig-productNoProject-600-c]]
.Product Management Sometimes does not Use Projects
image::images/2_04-productNoProject.png[Product-outcomes2, 600,,]

In our scenario, you are now on a tight-knit, collaborative team. You should think in terms of developing and sustaining a product. However, projects still exist, and sometimes you may find yourself on a team that is funded and operated on that basis. You also will encounter the concept of “process” even on a single team; more on that in xref:KLP-work-management[]. We will go further into projects and process management in Context III.

[[KLP-amazon-productization]]
===== Productization as a Strategy at Amazon
(((product management, Amazon influences on)))(((Amazon, API mandate)))
(((two-pizza team)))(((Amazon, two-pizza team rule)))
((Amazon)) (the online retailer) is an important influence in the modern trend towards product-centric IT management. First, all Amazon teams were told to assume that the functionality being built might at some point be offered to external customers cite:[Lane2012].

Second, a widely reported practice at Amazon.com is the http://www.fastcompany.com/3037542/productivity-hack-of-the-week-the-two-pizza-approach-to-productive-teamwork[limitation of product teams to between five and eight people], the number that can be fed by “two pizzas” (depending on how hungry they are) cite:[Gillett2014]. It has long been recognized in software and IT management that larger teams do not necessarily result in higher productivity. The best known statement of this is "((Brooks' Law))” from The Mythical Man-Month, that “adding people to a late project will make it later” cite:[Brooks1975].

The reasons for “Brooks' Law” have been studied and analyzed (see, for example, cite:[Madachy2008, Choi2014]) but in general, it is due to the increased communication overhead of expanded teams. Product design work (of which software development is one form) is creative and highly dependent on tacit knowledge, interpersonal interactions, organizational culture, and other “soft” factors. Products, especially those with a significant IT component, can be understood as socio-technical systems, often complex. This means that small changes to their components or interactions can have major effects on their overall behavior and value.

This, in turn, means that newcomers to a product development organization can have a profound impact on the product. Getting them “up to speed” with the culture, mental models, and tacit assumptions of the existing team can be challenging and rarely is simple. And the bigger the team, the bigger the problem. The net result of these two practices at Amazon (and now xref:fowler-quote[General Electric and many other companies]) is the creation of multiple nimble services that are decoupled from each other, constructed and supported by teams appropriately sized for optimal high-value interactions.

Finally, Amazon founder Jeff Bezos mandated that all software development should be http://apievangelist.com/2012/01/12/the-secret-to-amazons-success-internal-apis/[service-oriented]. That means that some form of standard xref:application-programming-interface[API] was required for all applications to communicate with each other. Amazon's practices are a clear expression of xref:KLP-cloud-native[cloud-native] development.

*Evidence of Notability*

Product management has a dedicated professional association, the ((Product Development and Management Association)) (www.pdma.org.) Notable authors include Steve Blank, Marty Cagan, and Jeff Gothelf. The topic as a whole is closely related to the general topic of R&D. There are many meetups, conferences, and other events held under various banners such as Agile development.

*Limitations*

Product management tends to assume the existence of a market, and customers whose reaction is unpredictable. This is not always the case in digital systems. Sometimes, digital artifacts and capabilities have greater constraints, and must follow established specifications.


*Related Topics*

* xref:KLP-digital-xform[Digital Value]
* xref:KLP-app-deliv[Application Delivery]
* xref:KLP-ops-mgmt[Operational Component]
* xref:chap-invest-mgmt[Investment]
* xref:chap-arch-portfolio[Architectural Coordination]
