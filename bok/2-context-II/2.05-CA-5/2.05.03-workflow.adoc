
// layout intentional below of anchors
[[KLP-workflow]]
==== Work Management Capabilities and Approaches
[[ticketing]]

*Description*

(((ticketing))) As a digital product starts to gain a user base, and as a company matures and grows, there emerges a need for human-to-human support. This is typically handled by a ((help desk)) or ((service desk)), serving as the human face of IT when the IT systems are not meeting people's expectations. We were first briefly introduced to the concept in our Service Lifecycle (see <<fig-ITStates-600-c>>).
//this won't preview but will render correctly

The service desk is an ((interrupt-driven)), task-oriented capability. It serves as the first point of contact for IT services that require some human support or intervention. As such, its role can become broad from ((provisioning)) access to assisting users in navigation and usage, to serving as an alert channel for outage reporting. The service desk ideally answers each user's request immediately, requiring no follow-up. If follow-up is required, a “ticket” is “issued”.

As a “help desk”, it may be focused on end-user assistance and reporting ((incident))s. As a “service desk”, it may expand its purview to accepting provisioning or other requests of various types (and referring and tracking those requests). Note that in some approaches, service request and incident are considered to be distinct processes.

The term "((ticket))” dates to paper-based industrial processes, where the “help desk” might actually be a physical desk, where a user seeking services might be issued a paper ticket. Such “tickets” were also used in field services.

In IT-centric domains, tickets are virtual; they are records in databases, not paper. The user is given a ticket “ID” or “number” for tracking (e.g., so they can inquire about the request's status). The ticket may be “routed” to someone to handle, but again in a virtual world what really happens is that the person it is routed to is directed to look at the record in the database. (In paper-based processes, the ticket might well be moved physically to various parties to perform the needed work.)

A service desk capability needs:

* Channels for accepting contacts (e.g., telephone, email, chat)
* Staffing appropriate to the volume and nature of those requests
* Robust ((workflow)) capabilities to track their progress
* ((Routing)) and ((escalation)) mechanisms, since clarifying the true nature of contacts and getting them serviced by the most appropriate means are non-trivial challenges

Work management in practice has divided between development and operations practices and tools. However, ((DevOps)) and ((Kanban)) are forcing a reconsideration and consolidation. Historically, here are some of the major tools and channels through which tasks and work are managed on both sides:

// break page for pdf
ifdef::backend-pdf[<<<]
.Dev _versus_ Ops Tooling
[cols="2*", options="header"]
|====
|Development|Operations
|User story tracking system|Service or help desk ticketing system
|Issue/risk/action item log|Incident management system
|Defect tracker|Change management system
|====

All of these systems have common characteristics. All can (or should) be able to:

* Register a new task
* Describe the work to be done (development or break/fix/remediate)
* Represent the current status of the work
* Track who is currently accountable for it (individual and/or team)
* Indicate the priority of the work, at least in terms of a simple categorization such as high/medium/low

More advanced systems may also be able to:

* Link one unit of work to another (either as parent/child or peer-to-peer)
* Track the effort spent on the work
* Prioritize and order work
* Track the referral or escalation trail of the work, if it is routed to various parties
* Link to communication channels such as conference bridges and paging systems

The first automated system (computer-based) you may find yourself acquiring along these lines is a help desk system. You may be a small company, but when you start to build a large customer base, keeping them all happy requires more than a manual, paper-based card wall or Kanban board.

[[KLP-process-mgmt-emerges]]
==== Towards Process Management

The Kanban board has started to get complicated (see <<fig-complexKanban-800-c>>, loosely based on Image from cite:[Kos2016]). We are witnessing an increasing amount of work that needs to follow a sequence, or checklist, for the sake of consistency.

[[fig-complexKanban-800-c]]
.Medium-Complex Kanban Board
image::images/2_05-complexKanban.png[complex Kanban, 800,,]

Process management is when we need to start managing:

* Multiple
* Repeatable
* Measurable sequences of activity
* Considering their interdependencies
* Perhaps using common methods to define them
* And even common tooling to support multiple processes

===== Process Basics

We have discussed some of the factors leading to the need for process management, but we have not yet come to grips with what it *is*. To start, think of a repeatable series of activities, such as when a new employee joins (see <<fig-orderingA-600-c>>).

[[fig-orderingA-600-c]]
.Simple Process Flow
image::images/2_05-orderingA.png[flow steps, 600,,]

Process management can represent ((conditional logic)) (see <<fig-orderingB-600-c>>).

[[fig-orderingB-600-c]]
.Conditionality
image::images/2_05-orderingB.png[flow steps, 600,,]

Process models can become extremely intricate, and can describe both human and automated activity. Sometimes, the process simply becomes too complicated for humans to follow. Notice how different the process models are from the card wall or Kanban board. In Kanban, everything is a work item, and the overall flow is some simple version of “to do, doing, done”. This can become complex when the flow gets more elaborate (e.g., various forms of testing, deployment checks, etc.). In a process model, the activity is explicitly specified on the assumption it will be repeated. The boxes representing steps are essentially equivalent to the columns on a Kanban board, but since sticky notes are not being used, process models can become very complex — like a Kanban board with dozens or hundreds of columns! Process management as a practice is discussed extensively in Context III. However, before we move on, two simple variations on process management are:

* Checklists
* Case Management

[[checklist-manifesto]]

===== The Checklist Manifesto

((("Gawande, Atul"))) ((("Checklist Manifesto, The")))_The Checklist Manifesto_ is the name of a notable book by author/surgeon Atul Gawande cite:[Gawande2010]. The title can be misleading; the book in no way suggests that all work can be reduced to repeatable checklists. Instead, it is an in-depth examination of the relationship between standardization and complexity. Like Case Management, it addresses the problem of complex activities requiring professional judgment.

Unlike Case Management (discussed below), it explores more time-limited and often urgent activities such as flight operations, large-scale construction, and surgery. These activities, as a whole, cannot be reduced to one master process; there is too much variation and complexity. However, within the overall bounds of flight operations, or construction, or surgery, there are critical sequences of events that _must_ be executed, often in a specific order. Gawande discusses the airline industry as a key exemplar of this. Instead of one “master checklist” there are specific, clear, brief checklists for a wide variety of scenarios, such as a cargo hold door becoming unlatched.

There are similarities and differences between core BPM approaches and checklists. Often, BPM is employed to describe processes that are automated and whose progress is tracked in a database. Checklists, on the other hand, may be more manual, intended for use in a closely collaborative environment (such as an aircraft cockpit or operating room), and may represent a briefer period of time.

Full process management specifies tasks and their flow in precise detail. We have not yet got to that point with our Kanban board, but when we start adding checklists, we are beginning to differentiate the various processes at a detailed level. We will revisit Gawande's work in Context III with the coordination technique of the xref:submittal-schedule[submittal schedule].

[[case-mgmt]]
===== Case Management

[[fig-casemgmt-600-c]]
.Process Management _versus_ Case Management
image::images/2_05-casemgmt.png[case management, 600]

NOTE: Do not confuse “Case” here with Computer Assisted Software Engineering.

(((case management))) Case Management is a concept used in medicine, law, and social services. Case Management can be thought of as a high-level process supporting the skilled knowledge worker applying their professional expertise. Cases are another way of thinking about the relationship between the Kanban board and process management (see <<fig-casemgmt-600-c>>).


.Workflow Management Coalition on Case Management
****
Business Process Modeling and Case Management are useful for different kinds of business situations:

* Highly predictable and highly repeatable business situations are best supported with BPM
** For example, signing up for a cell phone service: it happens thousands of times a day, and the process is essentially fixed
* Unpredictable and unrepeatable business situations are best handled with Case Management
** For example, investigation of a crime will require following up on various clues, down various paths, which are not predictable beforehand; there are various tests and procedures to use, but they will be called only when needed

cite:[WFMC2010], via cite:[England2013]
****
((("England, Rob"))) IT consultant and author Rob England contrasts “Case Management” with “Standard Process” in his book _Plus! The Standard+Case Approach: See Service Response in a New Light_ cite:[England2013]. Some processes are repeatable and can be precisely standardized, but it is critical for anyone working in complex environments to understand the limits of a standardized process. Sometimes, a large “case” concept is sufficient to track the work. The downside may be that there is less visibility into the progress of the case — the person in charge of it needs to provide a status that can't be represented as a simple report. We will see process management again in xref:KLP-ops-mgmt[] in our discussion of xref:KLP-IT-process-emergence[operational process emergence].

*Evidence of Notability*
Workflow management in the basic emergent sense is a key precursor to full BPM. See, for example, cite:[Sharp2009].

*Limitations*
Not all work can or should be reduced to a procedural paradigm. Higher-touch, more variable services and R&D work require different approaches, such as Case Management.


*Related Topics*

* xref:KLP-product-team-approaches[Product Team Practices]
* xref:KLP-lean-work-mgmt[Lean Management]
* xref:KLP-lean-product-dev[Lean Product Development]
* xref:KLP-ops-response[Operational Response]
* xref:KLP-chap-coordination[Coordination and Process]
* xref:KLP-organization[Organizational Structure]
* xref:gov-elements[Governance Elements]
