=== Preface

==== The Open Group

The Open Group is a global consortium that enables the achievement of business objectives through technology standards. Our diverse membership of more than 700 organizations includes customers, systems and solutions suppliers, tools vendors, integrators, academics, and consultants across multiple industries.

The mission of The Open Group is to drive the creation of Boundaryless Information Flow(TM) achieved by:

* Working with customers to capture, understand, and address current and emerging requirements, establish policies, and share best practices
* Working with suppliers, consortia, and standards bodies to develop consensus and facilitate interoperability, to evolve and integrate specifications and open source technologies
* Offering a comprehensive set of services to enhance the operational efficiency of consortia
* Developing and operating the industry’s premier certification service and encouraging procurement of certified products

Further information on The Open Group is available at http://www.opengroup.org/[www.opengroup.org].

The Open Group publishes a wide range of technical documentation, most of which is focused on development of Standards and Guides, but which also includes white papers, technical studies, certification and testing documentation, and business titles. Full details and a catalog are available at http://www.opengroup.org/library[www.opengroup.org/library].

==== This Document

This document is the Digital Practitioner Body{nbsp}of{nbsp}Knowledge(TM) Community Edition, also known as the DPBoK(TM) Community Edition. It forms the core of the Digital Practitioner Body of Knowledge Standard maintained and published by The Open Group. See xref:Introduction[] for a detailed description of the Community Edition and its relationship with the standard.

==== Background and Intended Value of this Work

Applied computing, now popularly termed "`digital technology`", is transforming economies and societies worldwide. Digital investments are critical for modern organizations. Participating in their delivery (i.e., working to create and manage them for value) can provide prosperity for both individuals and communities. Computing programs worldwide are under pressure to produce an increasing number of qualified professionals to meet voracious workforce demand. And skill requirements have undergone a seismic shift over the past 20 years. Digital Practitioners require a wide variety of skills and competencies, including cloud architecture and operations, continuous delivery and deployment, collaboration, Agile and Lean methods, product management, and more.

Industry guidance has over the years become fragmented into many overlapping and sometimes conflicting bodies of knowledge, frameworks, and industry standards. The emergence of Agile cite:[Alliance2001] and DevOps cite:[Kim2013] as dominant delivery forms has thrown this already fractured ecosystem of industry guidance into chaos. Organizations with longstanding commitments to existing bodies of knowledge are re-assessing those commitments. Changes in digital delivery are happening too fast for generational turnover to suffice.

Mid-career IT professionals, who still anticipate many more years in the workforce, are especially at risk. Learning the new "`digital`" approaches is not optional for them. But how to reconcile these new practices with the legacy "`best practices`" that characterized these workers' initial professional education? Now is the time to re-assess and synthesize new guidance reflecting the developing industry consensus on how digital and IT professionals should approach their responsibilities. Modern higher education is not keeping pace in these topics either. There has been too much of a gap between academic theory and classroom instruction _versus_ the day-to-day practices of managing digital products.

The Digital Practitioner in today's work environment thus encounters a confusing and diverse array of opinions and diverging viewpoints. This document aims to provide a foundational set of concepts for the practitioner to make sense of the landscape they find in any organization attempting to deliver digital products. It strives to put both old and new in a common context, with well-supported analysis of professional practice. Practically, it should be of value for both academic and industry training purposes.

In conclusion: this document is intended broadly for the development of the Digital Practitioner or professional. It seeks to provide guidance for both new entrants into the digital workforce as well as experienced practitioners seeking to update their understanding on how all the various themes and components of digital and IT management fit together in the new world.

==== Curation Approach

===== Relationship of this Document to Other BoKs

This document may source knowledge from other BoKs. One of the reasons for the existence of this document is that a constellation of new best practices and approaches based on cloud, Agile, Lean, and DevOps is overtaking older approaches based on physical infrastructure, project management, process management, and functional specialization. The _Phoenix Project_ cite:[Kim2013] is a useful introduction to the new approaches; evidence of their effectiveness can be seen in the publicly available videos of practitioner case studies presented at the DevOps Enterprise Summit.

===== Interpretive Aspects

This document should not merely be an assemblage of other sources, however. It may include well-grounded synthesis and interpretation of the curated source material. See xref:dpbok-principles[the DPBoK principles] for further information.

===== Evidence of Notability

In the current fast-paced digital economy, curating notable and rigorous work by individuals on a fair-use basis into the standard seems advisable.

This will require an ongoing discussion and debate as to relevance and notability of the material. DevOps, design thinking, Agile methods, Site Reliability Engineering (SRE), and many other concepts have emerged of late. How do we know that they are notable and relevant? That they have staying power and merit inclusion? A proposed set of heuristics follows:

* Existence of an organized community - is there evidence for a concept's interest in terms of practitioners self-identifying under its banner and choosing to spend their resources attending local, national, or international events?

* Notable publications - are books in print on the topic from reputable publishers, for example, O'Reilly or Addison-Wesley, and are they garnering reviews on Amazon or Goodreads?

* Media and analyst coverage - there is an active community of professional commentary and analysis; its attention to a given topic is also evidence of notability - social media attention is an important, but not conclusive, subset of this class of evidence (it can be too easily manipulated)

The use of a given BoK or other guidance as broadly used audit criteria (e.g., cloud provider compliance) shall be construed as evidence of notability.
