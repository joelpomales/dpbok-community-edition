
==== Information Management Topics

*Description*

===== Social, Mobile, Analytics, and Cloud

(((SMAC)))(((social media)))(((mobile)))(((analytics)))(((cloud computing)))Discussions of Digital Transformation often reference the algorithm SMAC:

* Social
* Mobile
* Analytics
* Cloud computing

Others would add IoT. These are not equivalent terms; in fact, they have relationships to each other (see <<fig-SMAC-500-c>>).

[[fig-SMAC-500-c]]
.Social, Mobile, Analytics, and Cloud
image::images/4_11-SMAC.png[Social mobile analytics cloud, 500]

Social media is generally external to an organization and manifests as a form of xref:commercial-data[commercial data], that provides essential insights into bow a company's products are performing and being received.

Mobile (or mobility) has two distinct aspects: mobility as an engagement platform (e.g., for deployment of “apps” as one product form), _versus_ the xref:commercial-data[commercial data] available from mobile carriers, notably geolocation data.

IoT can be either an internal or external data source, often extremely high volume and velocity, requiring analysis services for sense-making and value extraction.

===== Big Data

The term "((Big Data))” in general refers to data that exceeds the traditional data management and data warehousing approaches we have discussed above.

As proposed by analyst Doug Laney in 2001 cite:[Laney2001], its most well-known definition identifies three dimensions of scale:

* Volume
* Variety
* Velocity

For example, high-volume data is seen in the search history logs of search engines such as Google.

High-variety data encompasses rich media and unstructured data, such as social media interactions.

High-velocity data includes telemetry from IoT devices or other sources capable of emitting large volumes of data very quickly.

All of these dimensions require increasingly specialized techniques as they scale, and the data management product ecosystem has continued to diversify as a result.

===== Managing the Information of Digital Delivery

NOTE: We have talked about xref:commit-as-metadata[metadata] previously, and xref:biz-impact-CMDB[understanding business impact with the CMDB]. You should review that material before continuing.

(((metadata)))(((CMDB)))Regarding our previous data warehousing architecture, the digital pipeline can be seen as related to four areas (see <<fig-digitalPipeline-500-c>>):

* Product management
* IT
* Support
* Operations

[[fig-digitalPipeline-500-c]]
.The Data Architecture of Digital Management
image::images/4_11-digitalPipeline.png[digital pipeline, 500]

Assume the primary product of the organization is an information-centric digital service, based ultimately on data. How do you manage data? How do you manage anything? In part, through collecting data about it. Wait — “data about data"? There's a word for that: *metadata*. We will take some time examining it, and its broader relationships to the digital delivery pipeline. The association of business definitions with particular data structures is one form of metadata. Data governance, records management, and ongoing support for digital consumers all require some layer of interpretation and context to enrich the raw data resource.

Consider the following list:

.Servers and databases
[cols="2*", options="header"]
|====
|Server|Database
|SRV001|DB0023
|SRV001|DB0045
|SRV002|DB0067
|====

Not very useful, is it? Compare it to:

.Servers, databases, product, and governance information
[cols="4*", options="header"]
|====
|Server|Database|Product|Regulatory
|SRV001|DB0023|Online reviews|Customer privacy
|SRV001|DB0045|Employee records management|HIPAA, PII
|SRV002|DB0067|Online sales|PCI, PII
|====

We could also include definitions of the tables and columns held in each of those databases. However, what system would contain such data? There have been a couple of primary answers over the years: metadata repositories and CMDBs.

In terms of this document's general definition of xref:commit-as-metadata[metadata] as non-runtime information related to digital assets, both the metadata repository and the CMDB contain metadata. However, they are not the only systems in which data related to digital services is seen. Other systems include monitoring systems, portfolio management systems, risk and policy management systems, and much more. All of these systems themselves can be aggregated into a data warehousing/BI closed-loop infrastructure.

This means that the digital organization, including organizations transforming from older IT-centric models, can apply Big Data, machine learning, text analytics, and the rest of the techniques and practices covered in this Competency Category.

NOTE: We will examine a full “business of IT/digital delivery pipeline” architecture in the next Competency Category.

*Evidence of Notability*

Considered in each specific topic area.

*Limitations*

Considered in each specific topic area.

// tag::learning-outcomes[]
// break page for pdf
ifdef::backend-pdf[<<<]
*Competency Category "Topics in Information Management" Example Competencies*

* Identify the themes of social, mobile, analytics, and cloud as modern digital channels and platforms
* Identify the data management concerns of the digital pipeline itself

// end::learning-outcomes[]


*Related Topics*

* xref:KLP-digital-xform[Digital Value]
* xref:KLP-enterprise-info-mgmt[Enterprise Information Management]

ifdef::collaborator-draft[]

===== Additional Topics
 ====== Data science
 ====== Semantic web and ontologies
 ====== ERP systems (special section??)
 ====== Business rules management
 ====== Knowledge mgmt
 ====== Information lifecycle (POSMAD)
 McGilvary (based on English) pp.23-24 — perhaps into 4.11.03.
 ====== Non-invasive data governance (Seiner?)

 ===== Data and capacity
 [quote, Abbott & Fisher, The Art of Scalability]
 In our experience, most companies simply do not pay attention to the deteriorating value of data and the increasing cost of maintaining increasing amounts of data over time.

* Options value
* Strategic value
* Cost justification
* Transform/reduce

 Schema.org & phonetics transcription from CACM 02-2106

 Some database professionals still don’t like virtualization; that is, making them run an Oracle RDBMS or Microsoft SQL Server on top of virtual servers, instead of directly on the host OS. The will say that “the database IS virtualization”.

endif::collaborator-draft[]
