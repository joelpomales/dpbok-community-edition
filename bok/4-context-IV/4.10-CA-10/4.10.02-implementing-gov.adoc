
ifdef::backend-pdf[<<<]
[[gov-elements]]
==== Implementing Governance

To govern, organizations must rely on a variety of mechanisms, which are documented in this competency category.

===== Elements of a governance structure

Organizations leverage a variety of mechanisms to implement governance. The following draws on COBIT 2019 and other sources cite:[ISACA2012d, ISACA2018a]:

* Mission, Principles, Policies, and Frameworks
* Organizational Structures
* Culture, Ethics, and Behavior
* People, Skills, and Competencies
* Processes and Procedures
* Information
* Services, Infrastructure, and Applications

NOTE: COBIT terminology has changed over the years, from "control objectives" (v4 and before) to "enablers" (v5) to the current choice of "components". This document uses the term "elements", and retains frameworks as a key governance element (COBIT 2019 dropped).

In xref:fig-elements-600-c[] varying lengths of the elements are deliberate. The further upward the bar extends, the more they are direct concerns for governance. All of the elements are discussed elsewhere in the book.

[[fig-elements-600-c]]
.Elements Across the Governance Interface
image::images/4_10-gov-elements.png[Governance elements, 600]

.Governance element cross-references
[cols="2*", options="header"]
|====
|Element|Covered
|Principles, Policies, and Procedures|Principles & policies covered in this Competency Category. Frameworks covered in (xref:PMBOK[PMBOK]), (xref:CMMI[CMMI], xref:ITIL[ITIL], xref:COBIT[COBIT], xref:TOGAF[TOGAF]), (xref:DMBOK[DMBOK]).
|Processes|xref:KLP-chap-coordination[Competency Area 7]
|Organizational Structures|xref:chap-org-culture[Competency Area 9 on org structure]
|Culture, Ethics, and Behavior|xref:KLP-culture[Competency Area 9 on culture]
|Information|xref:chap-ent-info-mgmt[Competency Area 11]
|Services, Infrastructure, and Applications|xref:Context-I[Context I]
|People, Skills, and Competencies|xref:KLP-people-mgmt[Competency Area 9 on workforce]
|====

Here, we are concerned with their aspect as presented to the governance interface. Some notes follow.

[[policy-hierarchy]]
===== Mission, Principles, Policies, and Frameworks

[quote, Michael Griffin, “How To Write a Policy Manual"]
Carefully drafted and ((standardized policies)) and procedures save the company countless hours of management time. The consistent use and interpretation of such policies, in an evenhanded and fair manner, reduces management's concern about legal issues becoming legal problems.

(((policies))) (((frameworks)))Principles (((principles))) are the most general statement of organizational vision and values. Policies will be discussed in detail in the next section. In general, they are binding organization mandates or regulations, sometimes grounded in external laws. Frameworks were defined in xref:KLP-frameworks[]. We discuss all of these further in this Competency Category.

NOTE: Some companies may need to institute formal policies quite early. Even a startup may need written policies if it is concerned with regulations such as ((HIPAA)). However, this may be done on an _ad hoc_ basis, perhaps outsourced to a consultant. (A startup cannot afford a dedicated VP of Policy and Compliance.) This topic is covered in detail in this section because, at enterprise scale, ongoing policy management and compliance must be formalized. Recall that xref:formalization[formalization] is the basis of our emergence model.

====== Vision Hierarchy

[[fig-policy-600-c]]
.Vision/Mission/Policy Hierarchy
image::images/4_10-policy.png[policy hierarchy, 600]

Illustrated in <<fig-policy-600-c>> is one way to think about policy in the context of our overall governance objective of value recognition.

The organization's *((vision and mission))* should be terse and high level, perhaps something that could fit on a business card. It should express the organization's reason for being in straightforward terms. Mission is the reason for being; vision is a “picture” of the future, preferably inspirational.

The *((principles and codes))* should also be brief. (“Codes” can include codes of ethics or codes of conduct.) For example, Nordstrom's is about 8,000 words, perhaps about ten pages.

[[how-policy-begins]]
*Policies* are more extensive. There are various kinds of policies:

(((verticals, banking)))In a non-IT example, a compliance policy might identify the ((Foreign Corrupt Practices Act)) and make it clear that bribery of foreign officials is unacceptable. Similarly, a human resources policy might spell out acceptable and unacceptable side jobs (e.g., someone in the banking industry might be forbidden from also being a mortgage broker on their own account).

Policies are often independently maintained documents, perhaps organized along lines similar to:

* Employment and human resources policies
* Whistleblower policy (non-retaliation)
* Records retention
* Privacy
* Workplace guidelines
* Travel and expense
* Purchasing and vendor relationships
* Use of enterprise resources
* Information security
* Conflicts of interest
* Regulatory

(This is not a comprehensive list.)

Policies, even though more detailed than codes of ethics/conduct, still should be written fairly broadly. In many organizations, they must be approved by the Board of Directors. *They should, therefore, be independent of technology specifics.* An information security policy may state that the ((hardening guidelines)) must be followed, but the hardening guidelines (stipulating, for example, what services and open ports are allowable on Debian(R) Linux) are _not_ policy. There may be various levels or classes of policy.

Finally, policies reference *((standards and processes))* and other governance elements as appropriate. This is the management level, where documentation is specific and actionable. Guidance here may include:

* Standards
* Baselines
* Guidelines
* Processes and procedures

((("Harris, Shon")))These concepts may vary according to organization, and can become quite detailed. Greater detail is achieved in hardening guidelines. A behavioral baseline might be “Guests are expected to sign in and be accompanied when on the data center floor.” We will discuss technical baselines further in the Competency Category on security, and also in our discussion of the technology product lifecycle in xref:chap-arch-portfolio[]. See also Shon Harris' excellent _((CISSP)) Exam Guide_ cite:[Harris2013] for much more detail on these topics.

The ideal end state is a policy that is completely traceable to various automation characteristics, such as approved “Infrastructure as Code” settings applied automatically by configuration management software (as discussed in “The DevOps Audit Toolkit,” cite:[DeLuccia2015] — more on this to come). However, there will always be organizational concerns that cannot be fully automated in such manners.

((("Schlarman, Steve")))Policies (and their implementation as processes, standards, and the like) must be enforced. As Steve Schlarman notes “Policy without a corresponding ((compliance)) measurement and monitoring strategy will be looked at as unrealistic, ignored dogma.” cite:[Schlarman2008]

Policies and their derivative guidance are developed, just like systems, via a lifecycle. They require some initial vision and an understanding of what the requirements are. Again, Schlarman: “policy must define the why, what, who, where, and how of the IT process” cite:[Schlarman2008]. User stories have been used effectively to understand policy needs.

Finally, an important point to bear in mind:

_Company policies can breed and multiply to a point where they can hinder innovation and risk-taking. Things can get out of hand as people generate policies to respond to one-time infractions or out of the ordinary situations_ cite:[Griffin2016(17)].

It is advisable to institute ((sunset dates)) or some other mechanism that forces their periodic review, with the understanding that any such approach generates ((demand)) on the organization that must be funded. We will discuss this more in the Competency Category on digital governance.

[[innovation-cycle]]
====== Standards, Frameworks, Methods, and the Innovation Cycle

(((standard, defined)))We used the term “standards” above without fully defining it. We have discussed a variety of industry influences throughout this document: PMBOK, ITIL, COBIT, Scrum, Kanban, ISO/IEC 38500, and so on. We need to clarify their roles and positioning further. All of these can be considered various forms of “guidance” and as such may serve as xref:gov-elements[governance elements]. However, their origins, stakeholders, format, content, and usage vary greatly.

First, the term "*standard*” especially has multiple meanings. A “standard” in the policy sense may be a set of compulsory rules. Also, “standard” or “baseline” may refer to some intended or documented state the organization uses as a reference point. An example might be “we run Debian Linux 16_10 as a standard unless there is a compelling business reason to do otherwise”.

This last usage shades into a third meaning of uniform, _de jure_ standards such as are produced by the ((IEEE)), ((IETF)), and ((ISO/IEC)).

* ISO/IEC: International Standards Organization/International Electrotechnical Commission
* IETF: Internet Engineering Task Force
* IEEE: Institute of Electrical and Electronics Engineers

(((International Standards Organization)))(((Organization for International Standardization)))The International Standards Organization, or ISO, occupies a central place in this ecosystem. It possesses “general consultative status” with the United Nations, and has over 250 technical committees that develop the actual standards.

The IEEE standardizes such matters as wireless networking (e.g., WiFi)(((WiFi))). The IETF (Internet Engineering Task Force) standardizes lower-level Internet protocols such as ((TCP/IP)) and ((HTTP)). The ((W3C)) (World Wide Web Consortium) standardizes higher-level Internet protocols such as ((HTML)). Sometimes standards are first developed by a group such as the IEEE and then given further authority though publication by ISO/IEC. The ISO/IEC in particular, in addition to its technical standards, also develops higher-order management/"((best practice))” standards. One well-known example of such an ISO standard is the ((ISO 9000)) series on quality management.

Some of these standards may have a great effect on the digital organization. We will discuss this further in the Competency Category on compliance.

(((frameworks, software _versus_ process)))(((software, frameworks)))Frameworks were discussed in xref:KLP-frameworks[]. Frameworks have two major meanings. First, software frameworks are created to make software development easier. Examples include ((Struts)), ((AngularJS)), and many others. This is a highly volatile area of technology, with new frameworks appearing every year and older ones gradually losing favor.

In general, we are not concerned with these kinds of specific frameworks in this document, except governing them as part of the xref:tech-prod-lifecycle[technology product lifecycle](((technology product lifecycle))). We are concerned with “process” frameworks such as ITIL, PMBOK, COBIT, CMMI, and the TOGAF framework. These frameworks are not “standards” in and of themselves. *However*, they often have corresponding ISO standards:

.Frameworks and Corresponding Standards
[cols="2*", options="header"]
|====
|Framework|Standard
|((ITIL))|((ISO/IEC 20000))
|((COBIT))|((ISO/IEC 38500))
|((PMBOK))|((ISO/IEC 21500))
|((CMMI))|((ISO/IEC 15504))
|((TOGAF))|((ISO/IEC 42010))
|====

Frameworks tend to be lengthy and verbose. The ISO/IEC standards are brief by comparison, perhaps on average 10% of the corresponding framework. Methods (_aka_ methodologies) in general are more action-oriented and prescriptive. ((Scrum)) and ((XP)) are methods. It is at least arguable that PMBOK is a method as well as a framework.

NOTE: There is little industry consensus on some of these definitional issues, and the student is advised not to be overly concerned about such abstract debates. If you need to comply with something to win a contract, it doesn't matter whether it's a “standard”, “framework”, “guidance”, “method”, or whatever.

(((frameworks, commercial incentives)))Finally, there are terms that indicate technology cycles, movements, communities of interest, or cultural trends: ((Agile)) and ((DevOps)) being two of the most current and notable. These are neither frameworks, standards, nor methods. However, commercial interests often attempt to build frameworks and methods representing these organic trends. Examples include SAFe, Disciplined Agile Delivery, the DevOps Institute, the DevOps Agile Skills Association, and many others.

[[innovation-cycle]]

[[fig-standards-cycle-500-c]]
.Innovation Cycle
image::images/4_10-standards-cycle.png[standards cycle, 500]

_Figure:_ <<fig-standards-cycle-500-c>> illustrates the ((innovation cycle)). Innovations produce value, but innovation presents change management challenges, such as cost and complexity. The natural response is to standardize for ((efficiency)), and ((standardization)) taken to its end state results in commodification, where costs are optimized as far as possible, and the remaining concern is managing the risk of the commodity (as either consumer or producer). While efficient, commoditized environments offer little competitive value, and so the innovation cycle starts again.

Note that the innovation cycle corresponds to the elements of xref:govarch[value recognition]:

* Innovation corresponds to Benefits Realization
* Standardization corresponds to Cost Optimization
* Commoditization corresponds to Risk Optimization


====== Organizational Structures

We discussed basic ((organizational structure)) in xref:chap-org-culture[]. However, governance also may make use of some of the scaling approaches discussed in xref:KLP-people-mgmt[]. Cross-organization (((cross-organizational coordination))) coordination techniques (similar to those discussed in xref:boundary-spanning[Boundary Spanning]) are frequently used in governance (e.g., cross-organizational ((coordinating committees)), such as an enterprise security council).

====== Culture, Ethics, and Behavior

Culture, (((culture))) ((ethics)), and ((behavior)) as a governance element can both drive revenue as well as risk and cost. See also xref:KLP-culture[Culture] and xref:KLP-people-mgmt[Hiring].

====== People, Skills, and Competencies

People and their ((skills)) and ((competencies)) (covered in xref:KLP-people-mgmt[]) are governance elements upon in which all the others rest. “People are our #1 asset” may seem to be a cliche, but it is ultimately true. Formal approaches to understanding and managing this base of skills are therefore needed. A basic “human resources” capability is a start, but sophisticated and ambitious organizations institute formal ((organizational learning)) capabilities to ensure that talent remains a critical focus.

====== Processes

Process is defined in xref:definitions[]. We will discuss ((processes as controls)) in the upcoming Competency Category on risk management. A control is a role that a governance element may play. Processes are the primary form of governance element used in this sense.

====== Information

Information is a general term; in the sense of a governance element, it is based on data in its various forms, with overlays of concepts (such as syntax and semantics) that transform raw “data” into a resource that is useful and valuable for given purposes. From a governance perspective, ((information)) carries governance direction to the governed system, and the fed back monitoring is also transmitted as information. Information resource management (((information resource management))) and related topics such as data governance and data quality are covered in xref:chap-ent-info-mgmt[]; it is helpful to understand governance at an overall level before going into these more specific domains.

====== Services, Infrastructure, and Applications

Services, (((services))) ((infrastructure)), and ((applications)) of course are the critical foundation of digital value. These fundamental topics were covered in xref:Context-I[Context I]. In the sense of governance elements, they have a recursive or self-reflexive quality. Digital technology automates business objectives; at scale, a digital pipeline becomes a non-trivial business concern in and of itself, requiring considerable automation cite:[Betz2011a], cite:[Open2015]. Applications that serve as digital governance elements might include:

* Source control (((source control)))
* Build management (((build management)))
* Package management (((package management)))
* Deployment and configuration management (((deployment management)))(((configuration management)))
* Monitoring (((monitoring)))
* Portfolio management (((portfolio management)))

*Evidence of Notability*

Governance requires mechanisms in order to function.

*Limitations*

Elaborate structures of governance elements (especially processes) can slow down digital delivery and, ironically, contribute greater risk than they reduce.


*Related Topics*

* xref:KLP-digital-xform[Digital Value]
* xref:KLP-process-mgmt[Process Management]
* xref:KLP-people-mgmt[Human Resources Management]
* xref:KLP-frameworks[Frameworks]
* xref:KLP-risk-management[Risk Management]
